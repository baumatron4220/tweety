@cannot ('edit', $user)
	<form method="POST"
		{{-- action="/profiles/{{ $user->username }}/follow" --}}
		action="{{ route('follow', $user->username) }}"
	>
		@csrf

		<button type="submit"
			class="bg-blue-500 rounded-full shadow py-2 px-2 text-white text-xs"
		>
			{{ auth()->user()->isFollowing($user) ? 'Unfollow Me' : 'Follow Me' }}
		</button>
	</form>
@endcannot
