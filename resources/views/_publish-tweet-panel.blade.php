<div class="border border-blue-400 rounded-lg py-6 px-8 mb-8">
    <form method="POST" action="/tweets">
    	@csrf

        <input name="body" class="w-full p-2" placeholder="What's up doc?"></input>

        <hr class="my-4">

        <footer class="flex justify-between">
            <img
                src="{{ auth()->user()->avatar }}"
                alt="your avatar"
                class="rounded-full mr-2"
                width="50"
                height="50"
                >
            <button type="submit" class="bg-blue-500 rounded-full shadow py-2 px-10 text-white">Publish</button>
        </footer>
    </form>

    @error('body')
    	<p class="text-red-500 text-sm mt-2">{{ $message }}</p>
    @enderror
</div>
