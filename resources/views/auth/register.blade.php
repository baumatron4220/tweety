<x-master>
    <div class="container mx-auto flex justify-center">
        <div class="px-12 py-8 bg-gray-200 border border-gray-300 rounded-lg">
            <div class="col-md-8">
                <div class="font-bold text-lg mb-4">{{ __('Register') }}</div>

                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="mb-6">
                        <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="username">
                            Username
                        </label>

                        <input type="text"
                            class="border-gray-400 p-2 w-full"
                            name="username"
                            id="username"
                            autocomplete="username"
                            value="{{ old('username') }}"
                            required
                            autofocus
                        >

                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-6">
                        <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="name">
                            Name
                        </label>

                        <input type="text"
                            class="border-gray-400 p-2 w-full"
                            name="name"
                            id="name"
                            autocomplete="name"
                            value="{{ old('name') }}"
                            required
                            autofocus
                        >

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-6">
                        <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="email">
                            Email
                        </label>

                        <input type="text"
                            class="border-gray-400 p-2 w-full"
                            name="email"
                            id="email"
                            autocomplete="email"
                            value="{{ old('email') }}"
                            required
                        >

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-6">
                        <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="password">
                            Password
                        </label>

                        <input type="password"
                            class="border-gray-400 p-2 w-full"
                            name="password"
                            id="password"
                            autocomplete="current-password"
                        >

                        @error('password')
                            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="mb-6">
                        <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="password_confirmation">
                            Confirm Password
                        </label>

                        <input type="password"
                            class="border-gray-400 p-2 w-full"
                            name="password_confirmation"
                            id="password-confirm"
                            autocomplete="new-password"
                        >
                    </div>

                    <div>
                        <button type="submit"
                            class="bg-blue-400 text-white roudned py-2 px-4 hover:bg-blue-500 mr-2"
                        >
                            Register
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</x-master>
