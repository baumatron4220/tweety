<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{
    /**
     * @param $user
     * @return mixed
     */
    public function show(User $user)
    {
        return view('profiles.show', compact('user'));
    }

    /**
     * @param $user
     * @return mixed
     */
    public function edit(User $user)
    {
        // if (current_user()->isNot($user)) {
        //     abort(404);
        // }

        // abort_if($user->isNot(current_user(), 404));

        // $this->authorize('edit', $user);

        // this is now done in the web routes using the user policy edit function...

        return view('profiles.edit', compact('user'));
    }

    /**
     * @param User $user
     */
    public function update(User $user)
    {
        $attributes = request()->validate([
            'username' => ['string', 'required', 'max:255', 'alpha_dash', Rule::unique('users')->ignore($user)],
            'name' => ['string', 'required', 'max:255'],
            'avatar' => ['file'],
            'email' => ['string', 'required', 'email', 'max:255', Rule::unique('users')->ignore($user)],
            'password' => ['string', 'required', 'min:8', 'max:255', 'confirmed'],
        ]);

        if (request('avatar')) {
            $attributes['avatar'] = request('avatar')->store('avatars');
        }

        $user->update($attributes);

        return redirect($user->path());
    }
}
