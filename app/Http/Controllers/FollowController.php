<?php

namespace App\Http\Controllers;

use App\User;

class FollowController extends Controller
{
    /**
     * @param User $user
     */
    public function store(User $user)
    {
        auth()->user()->toggleFollow($user);

        return back();
    }
}
