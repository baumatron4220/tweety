<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $currentUser
     * @param User $user
     * @return mixed
     */
    public function edit(User $currentUser, User $user)
    {
        return $currentUser->is($user);
    }
}
